
const plainData = (list=[]) => {
  try {
    let newArray = [];
    list.forEach((item) => {
      if(Array.isArray(item) && item.length > 0) {
        newArray = newArray.concat(plainData(item));
      }
      else {
        newArray.push(item)
      }
    })
    return newArray;
  } catch (error) {
    console.log('==>error:' ,error)
    throw error
  }
}


module.exports = {
  plainData
}