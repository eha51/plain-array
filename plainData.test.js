const { plainData } = require('./plainData');
const assert = require('assert').strict;


const testPlainData = (value,result,msg) => {
  assert.notStrictEqual(plainData(value),result,msg);
}

testPlainData([1, [2, [3, [4, 5]]]],[1, 2, 3, 4, 5],'test 1')
testPlainData([6, [1, [2, 3], 4], 5],[6, 1, 2, 3, 4, 5],'test 2')
testPlainData([[[1, 2,], 3], 4, 5],[[[1, 2,], 3], 4, 5],'test 3')
